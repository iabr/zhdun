# Zhdun utility

**zhdun** ia a command-line utility, that waits for an open port.
Key advantage: it does not have dependencies, may be copied directly in a Docker container.

```bash
if zhdun wait localhost 5432 30; then
    echo command returned true
else
    echo command returned some error
fi
```

## Docker compose example

```yaml
version: '3'

services:

  db:
    image: postgres:9.6
    restart: unless-stopped
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres

  test:
    depends_on:
      - db
    image: busybox
    restart: 'no'
    volumes:
      - ./bin/zhdun:/usr/bin/zhdun
    entrypoint: >
      sh -c "if /usr/bin/zhdun wait db 5432 10; then echo 'Ready!'; fi"
```
Try ```docker-compose up test```

## Building

Build a Linux version

```bash
./build.sh linux
```

Build a OSX version

```bash
./build.sh darwin
```

Build a Microsoft Windows version

```bash
./build.sh windows
```

Without this argument it will build a version for the current host operating system.

## Alternatives

```bash
echo "Connecting to database";
while ! nc -z db 5432; do echo "Connecting to database"; sleep 1; done;
echo "Connected!";
```

```bash
export PGPASSWORD=<password>;
echo "Connecting to database";
while ! pg_isready -h db -p 5432 -d <database> -U <user> -q; do echo "."; sleep 1; done;
echo "Connected!";
```