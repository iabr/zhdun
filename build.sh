#!/bin/bash

HDIR=`dirname $BASH_SOURCE`

export GOPATH=${PWD}

# go mod init
APP_NAME=zhdun

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     osName=Linux;;
    Darwin*)    osName=Mac;;
    CYGWIN*)    osName=Cygwin;;
    MINGW*)     osName=MinGw;;
    *)          osName="UNKNOWN:${unameOut}"
esac

if [ "${osName}" == "Cygwin" ] || [ "${osName}" == "MinGw" ]
  then
BUILD_OS=windows
fi

if [ "${osName}" == "Mac" ]
  then
BUILD_OS=darwin
fi

if [ "${osName}" == "Linux" ]
  then
BUILD_OS=linux
fi


if [ $# -eq 0 ] || [ -z "$1" ]
  then
    echo "No arguments supplied, building for $BUILD_OS"
    echo "./build.sh <windows|linux|darwin>"
    BUILD_FOR_OS=$BUILD_OS
  else
    BUILD_FOR_OS=$1
    echo "Building for $BUILD_FOR_OS"
fi

set -x

# get all packages
go get -d ${HDIR}/

# cobra has OS-specific dependencies

if [ BUILD_FOR_OS != BUILD_OS ]
  then
GOOS=${BUILD_FOR_OS} go get -u github.com/spf13/cobra
fi

# to reduce binary size
# see https://blog.filippo.io/shrink-your-go-binaries-with-this-one-weird-trick/
case "${BUILD_FOR_OS}" in
    linux*)      BUILD_FLAGS="-ldflags=\"-s -w\"";;
    darwin*)     BUILD_FLAGS="-ldflags=\"-s -w\"";;
    windows*)    BUILD_FLAGS="";;
    *)           BUILD_FLAGS=""
esac


APP_EXT=""
if [ "${BUILD_FOR_OS}" == "windows" ]
  then
    APP_EXT=".exe"
else
  if [ "${BUILD_FOR_OS}" == "darwin" ]
    then
      APP_EXT="_osx"
    fi
fi

APP_NAME=${APP_NAME}${APP_EXT}

echo $BUILD_FLAGS

mkdir -p bin
GOOS=${BUILD_FOR_OS} GOARCH=386 go build -o ${HDIR}/bin/${APP_NAME} ${HDIR}/main.go
# GOOS=${BUILD_FOR_OS} GOARCH=386 go build ${BUILD_FLAGS} -o ${HDIR}/bin/${APP_NAME} ${HDIR}/main.go
chmod +x ${HDIR}/bin/${APP_NAME}
