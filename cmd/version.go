package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// AppVersion is an application version, see https://semver.org/ for more details
var AppVersion = "0.1.0"

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version information",
	//Long:  `All software has versions. This is Hugo's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(AppVersion)
	},
}
