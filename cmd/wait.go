package cmd

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/spf13/cobra"
)

// checkPort verifies if port is opened.
// It returns true if the port is opened and false otherwise.
func checkPort(host string, port string, timeout time.Duration) bool {
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), timeout*time.Second)
	if err != nil {
		return false
	}
	if conn != nil {
		defer conn.Close()
		return true
	}

	return false
}

// WaitForPort waits for an opened port.
// It returns true if the port is opened and false otherwise, outputs messages depending on flag values.
func WaitForPort(host string, port string, timeout time.Duration, quietFlag bool, booleanFlag bool) bool {

	quietFlag = booleanFlag || quietFlag

	if !quietFlag {
		fmt.Print("Waiting for an open port.")
	}

	start := time.Now()

	reconnectionTimeout := time.Duration(3)

	for {
		if checkPort(host, port, reconnectionTimeout) {
			if !quietFlag {
				fmt.Println(" Port is open.")
			}

			if booleanFlag {
				fmt.Print("true")
			}

			return true
		}

		time.Sleep(reconnectionTimeout * time.Second)

		if !quietFlag {
			fmt.Print(".")
		}

		t := time.Now()
		elapsed := t.Sub(start) / time.Second

		if elapsed > timeout {
			if !quietFlag {
				fmt.Println(" Port is not open.")
			}
			break
		}
	}

	if booleanFlag {
		fmt.Print("false")
	}

	return false
}

// init adds waitCmd to the rootCmd.
func init() {
	rootCmd.AddCommand(waitCmd)
}

// waitCmd describes Cobra command.
var waitCmd = &cobra.Command{
	Use:   "wait <host> <port> <timeou in seconds> e.g. wait localhost 5432 30",
	Short: "Waits for specified port",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.Help()
			os.Exit(1)
		}

		var host = args[0]
		var port = args[1]
		timeoutInt64, _ := strconv.ParseInt(args[2], 10, 64)
		var timeout = time.Duration(timeoutInt64)

		if WaitForPort(host, port, timeout, quietFlag, booleanFlag) {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	},
}
