package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var quietFlag bool
var booleanFlag bool

func init() {
	rootCmd.PersistentFlags().BoolVarP(&quietFlag, "quiet", "q", false, "quiet, no output")
	rootCmd.PersistentFlags().BoolVarP(&booleanFlag, "boolean", "b", false, "boolean output")
}

var rootCmd = &cobra.Command{
	Use:   "zhdun",
	Short: "zhdun waits for an open port",
	Long: `zhdun ia a command-line utility

zhdun wait <host> <port> <timeou in seconds> e.g. wait localhost 5432 30"

Examples:

	Wait for an open port:
	zhdun wait localhost 5432 30
`,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) == 0 {
			cmd.Help()
			os.Exit(1)
		}

	},
}

//Execute runs all commands
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
