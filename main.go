// go run main.go
// compile for Linux:
// GOOS=linux GOARCH=386 go build -o zhdun main.go
// compile for OSX:
// GOOS=darwin GOARCH=386 go build -o zhdun main.go
// compile for Windows:
// GOOS=windows GOARCH=386 go build -o zhdun.exe main.go

// for windows dependencies execute
// GOOS=windows go get -u github.com/spf13/cobra

package main

import (
	"./cmd"
)

func main() {
	cmd.Execute()
}
